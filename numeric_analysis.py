__license__ = "MIT"
__author__ = "Luis Andrés García Contreras"
__startdate__ = "21/11/2020"
__module__ = "numeric_analysis"
__python_version__ = "3.8.5"
__numpy_version__ = "1.19.4"

__version__ = "0.1.1"


# Modules ---------------------------------------------------------------------------
import numpy as np
import math


# Local Functions -------------------------------------------------------------------

# P.S: More documentation is found in their respective tests


def getPoints(file):
    """Function that reads a txt file and gets the coordinates of a points."""

    points = []
    f = open(file, "r")

    n = int(f.readline())
    for i in range(n):
        temp = f.readline()
        temp = temp.replace("\n", "")
        temp = temp.split(" ")
        for j in range(len(temp)):
            temp[j] = float(temp[j])
        temp = tuple(temp)
        points.append(temp)

    f.close()

    return points


def trapezoidal_rule(f: lambda x: expression, a: int, b: int, sections: int = 4) -> float:
    """
    Function that returns the result (area) of an integral defined by the method of
    Trapezoidal rule. More sections, better answer.

    Parameters:
    ----------
        -f: function to integrate
        -a: lower limit
        -b: upper limit
        -sections: number of divisions 
    """

    h = (b-a)/sections
    xi = a
    temp_sum = f(xi)
    for i in range(0, sections-1, 1):
        xi = xi + h
        temp_sum = temp_sum + 2*f(xi)
    temp_sum = temp_sum + f(b)
    temp_sum = h*(temp_sum/2)

    return round(temp_sum, 6)


def lagrange_interpolation(points: list, xp: float) -> float:
    """
    This function helps us to fill incomplete data by method
    by lagrange.

     Parameters
     ---------
         -points: 
         -xp: the x of the missing data

     Note
     ----
    To avoid having to enter data by data within 'points', use the
    getPoints() function.
    """

    yp = 0
    for i in range(len(points)):
        p = 1
        for j in range(len(points)):
            if i != j:
                p = p * (xp - float(points[j][0])) / \
                    (float(points[i][0]) - float(points[j][0]))
        yp = yp + p * float(points[i][1])

    return round(yp, 6)


def secant(f: lambda x: expression, x0=0, x1=1, tol=0.0001, N=20) -> float:
    """ 
    Secant method to find the root of a function. Data input: xo, x1,
    tolerance, maximum number of iterations and function.

    Works on nonlinear equations

    Parameters
    ----------
        -f: the function of the nonlinear equation
        -x0: the first starting point
        -x1: the second starting point
        -tol: the margin of error
        -N: the number of iterations it has to do before it throws an exception

    Note
    ----
    You can uncomment the 3 lines below, so that you can also
    return the list of the number of iterations that the value is in it at that moment
    of x0 and x1.
    """

    #temp_list = [(0, x0)]
    i = 1
    while i <= N:
        x = x1 - (x1-x0)*f(x1)/(f(x1)-f(x0))
        #temp_list.append((i, x))
        if abs(x-x1) < tol:
            return round(x, 6)  # , temp_list
        i = i + 1
        x0 = x1
        x1 = x

    print('El metodo fracaso despues de %d iteraciones' % N)


def least_squares_straight_line(points: list) -> tuple:
    """
    Function that returns the corrected values of a series of points, using
    the method of least squares adjusting to a straight line.

    Parameters
    ----------
        points: 

    Note
     ----
    To avoid having to enter data by data within 'points', use the
    getPoints() function.
    """

    n = len(points)

    temp_dict = {
        "SumatoriaX": 0,
        "SumatoriaY": 0,
        "SumatoriaX^2": 0,
        "SumatoriaXY": 0
    }

    # conseguir sumatorias
    for i in range(n):
        temp_dict["SumatoriaX"] += float(points[i][0])
        temp_dict["SumatoriaY"] += float(points[i][1])
        temp_dict["SumatoriaX^2"] += (float(points[i][0])**2)
        temp_dict["SumatoriaXY"] += (float(points[i][0]) * float(points[i][1]))

    # conseguir a0 y a1
    a = [
        [n, temp_dict["SumatoriaX"]],
        [temp_dict["SumatoriaX"], temp_dict["SumatoriaX^2"]]
    ]
    b = [
        temp_dict["SumatoriaY"], temp_dict["SumatoriaXY"]
    ]
    x = seidel(a, b)

    # conseguir y corregir g(x) = a0 +a1x
    temp_list = []
    for i in range(n):
        temp_int = x[0] + (x[1]*float(points[i][0]))
        temp_list.append(round(temp_int, 6))

    return tuple(temp_list)


def seidel(a: list, b: list, iterations=25) -> tuple:
    """
    Function that returns the values in a linear equation in the form of
    tuple, using the Gauss-Seidel method.

    Parameters
    ----------
        a: List of equation 
        b: List of results
        iterations: The number of iterations

    Sources:
    ---------
    I need to research.
    """

    x = [0] * len(a)
    for i in range(0, iterations):

        n = len(a)
        for j in range(0, n):
            # temp variable d to store b[j]
            d = b[j]
            for i in range(0, n):
                if(j != i):
                    d -= a[j][i] * x[i]
            # updating the value of our solution
            x[j] = d / a[j][j]

    for i in range(len(x)):
        x[i] = round(x[i], 6)
    return tuple(x)


def rk4(f: lambda x, y: expression, y0: float, h: float, x0=0, steps=1) -> float:
    """
    This f returns the solution to an ordinary differential equation by
    the Runge-Kutta method 4th Order 1/3 of Simpson.

    The output of this Python program is the solution for dy / dx = x + y with the
    initial condition y = 1 for x = 0 that is, y (0) = 1 and we are trying to evaluate
    this differential equation at y = 1 using the RK4 method (Here y = 1 that is,
    and (1) =? is our calculation point).

    Parameters
    ---------
        f:
        y0:
        h: step size
        x0:
        steps: number of steps

    Sources
    -------
    'https://www.codesansar.com/numerical-methods/runge-kutta-fourth-order-rk4-python-program.htm'
    """

    for i in range(steps):
        k1 = h * (f(x0, y0))
        k2 = h * (f((x0+h/2), (y0+k1/2)))
        k3 = h * (f((x0+h/2), (y0+k2/2)))
        k4 = h * (f((x0+h), (y0+k3)))
        k = (k1+2*k2+2*k3+k4)/6
        yn = y0 + k
        y0 = yn
        x0 = x0+h

    return round(yn, 6)


def rk_higher_order(f: lambda x, y, delta_y: expression,
                    y0, h, delta_y0, x0=0, steps=1) -> float:
    """

    This function returns the value of the first derivative, transforming an equation
    higher order, by the Runge-Kutta method.

    delta_y0 = Vn - y0 = Un - x0 = qn

    Parameters
    ---------
        f: the function to evaluate
        y0:
        h: step size
        delta_y0:
        x0:
        steps: number of steps
    Note
    ----
    You need a clean function.

    Source
    ------
    Problemario Metodos Numericos de la Fime
    """

    for i in range(steps):
        k1 = h * delta_y0
        m1 = h * (f(x0, y0, delta_y0))
        k2 = h * (delta_y0 + m1)
        m2 = h * f(delta_y0+m1, y0+k1, x0+h)

        yn = y0+(0.5 * (k1+k2))
        delta_yn = delta_y0+(0.5 * (m1+m2))

        # Si es mas de un ciclo
        y0 = yn
        delta_y0 = delta_yn
        x0 = x0+h

    return round(delta_yn, 6)


def five_point_formula(f: lambda x: expression,
                       x0: int, h: float) -> float:
    """
    This function returns the approximation to the derivative of a first function
    order, with the five-point formula.

    Parameters
    ----------
        f: function to be derived
        x0:
        h: step size

    Sources
    -------
    Analisis Numerico (Richard L Burden) 7th
    """

    temp1 = (1 / (12*h))
    temp2 = (f(x0 - (2*h)) - (8*f(x0-h)) +
             (8*f(x0+h)) - f(x0 + (2*h)))
    return round(temp1*temp2, 6)


def extrapolation(points: list, xp: float) -> float:
    """
    This function helps us to know a point that is not in the intervals
    of a series of points. In other words with the data we have, we can
    predict values.

    Parameters
    ----------
        points:
        xp: the x of the missing data

    Note
    ----
    The points list must be in ascending order.

    Sources:
    I need to research.
    """

    m_prima = (points[-1][1]-points[0][1]) / (points[-1][0]-points[0][0])

    result = points[0][1] + (xp-points[0][0] * m_prima)

    return round(result, 6)

# Main ------------------------------------------------------------------------


def main():
    pass


# Main's end --------------------------------------------------------------------
if(__name__ == "__main__"):
    main()