import math
import numpy as np
import unittest
import numeric_analysis as nuan


# Classes ------------------------------------------------------------------------
class test_trapezoidal_rule(unittest.TestCase):
    """def setUp(self):
        print('This will run before each test')"""

    def test_valid_type(self):
        test_lambda = lambda x: np.sqrt(x)*np.sin(x)
        test_result = nuan.trapezoidal_rule(test_lambda, 1, 3)
        expected_result = 1.998417
        self.assertEqual(test_result, expected_result)

    """
    Que otros tipos de test se pueden hacer
        -test_invalid_type
        -test_none_input
        -test_negative_input
    """

    """def tearDown(self):
        print('this will run after each test')"""


class test_lagrange_interpolation(unittest.TestCase):

    def test_valid_type(self):
        points = [(2, 3.03), (2.5, 3.48), (3, 4.08), (3.5, 4.87)]
        test_result = nuan.lagrange_interpolation(points, 3.25)
        expected_result = 4.44875
        self.assertEqual(test_result, expected_result)


class test_secant(unittest.TestCase):

    def test_valid_type(self):
        test_lambda = lambda x: math.sqrt(x+2) - x + 3
        test_result = nuan.secant(test_lambda)
        expected_result = 5.791288
        self.assertEqual(test_result, expected_result)


class test_least_squares_straight_line(unittest.TestCase):

    def test_valid_type(self):
        points = [(1.1, 2.5), (1.9, 2.7), (2.4, 3.7), (4.8, 5.2)]
        test_result = nuan.least_squares_straight_line(points)
        expected_result = (2.429265, 3.034318, 3.412477, 5.227638)
        self.assertEqual(test_result, expected_result)


class test_seidel(unittest.TestCase):

    def test_valid_type(self):
        test_input_1 = [[4, 10.2], [10.2, 33.62]]
        test_input_2 = [14.1, 41.72]
        test_result = nuan.seidel(test_input_1, test_input_2)
        expected_result = (1.597316, 0.756317)
        self.assertEqual(test_result, expected_result)


class test_rk4(unittest.TestCase):

    def test_valid_type(self):
        test_lambda = lambda t,y: (2*t*y)-1
        test_result = nuan.rk4(test_lambda, 1, 0.6)
        expected_result = 0.666803
        self.assertEqual(test_result, expected_result)


class test_rk_higher_order(unittest.TestCase):

    def test_valid_type(self):
        test_lambda = lambda x,y,delta_y: (2*delta_y*x)-y
        test_result = nuan.rk_higher_order(test_lambda, 1.1, 0.2, 1.2)
        expected_result = 0.9952
        self.assertEqual(test_result, expected_result)


class test_five_point_formula(unittest.TestCase):

    def test_valid_type(self):
        test_lambda = lambda x: x * (math.e ** x)
        test_result = nuan.five_point_formula(test_lambda, 1, 0.6)
        expected_result = 5.361972
        self.assertEqual(test_result, expected_result)


class test_extrapolation(unittest.TestCase):

    def test_valid_type(self):
        test_input = [(2, 3.03), (2.5, 3.48), (3, 4.08), (3.5, 4.87)]
        test_result = nuan.extrapolation(test_input, 5.3)
        expected_result = 5.876667
        self.assertEqual(test_result, expected_result)


# ------------------------------------------------------------------------

if(__name__ == '__main__'):
    unittest.main()